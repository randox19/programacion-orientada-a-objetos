//Randall Zumbado Huertas
//2019082664



//Bibliotecas importadas
import java.util.Date;

//Clase Cuenta 
public class Cuenta{
  
  private int id; //Variable que contiene el ID
  private double balance; // Variable que contiene el balance de tipo Double
  private double tasaDeInteresAnual;//Variable que contiene la tasa de interes anual de tipo Double
  private Date fechaDeCreacion= new Date(); // Variable que contiene la fecha de tipo Date
  double retirar; // Variable que contiene el monto a retirar de tipo Double
  double deposito;// Variable que contiene el monto a depositar de tipo Double


  //Constructor de Cuenta 
  public Cuenta(){

    this.id=0;
    this.balance=0;
    this.tasaDeInteresAnual=0;
  
  
  }
  
  //Contructor que define el id y el balance de una cuenta
  public Cuenta(int id, double balance){

    this.id=id;
    this.balance=balance;
    
  }

  //Método para instanciar el resultado del id
  public int getid(){

    return this.id;
  }
  
  //Método para instanciar y definir el id
  public void setid(int id){

    this.id=id;
  }

  //Método para instanciar el resultado el id
  public double getbalance(){
    
    return this.balance;

  }

  //Método para instanciar y definir el id
  public void setbalance(double balance){

    this.balance=balance;
  }
  
  //Método para instanciar y definir el id
  public double gettasaDeInteresAnual(){

    return this.tasaDeInteresAnual;
  }

  //Método para instanciar y definir el id
  public void settasaDeInteresAnual(double tasaDeInteresAnual){

    this.tasaDeInteresAnual= tasaDeInteresAnual;
  }

  //Método para optener la fecha de creacion
  public Date getfechaDeCreacion(){
    
    return this.fechaDeCreacion;
  }

  //Método para obtener la tasa de interes mensual
  public double obtenerTasaDeInteresMensual(){
    double total;

    total=tasaDeInteresAnual / 12;

    return total;

  }
  
  //Método para calcular el interes mensual
  public double calcularInteresMensual(){
    double resultado;
    resultado= balance*tasaDeInteresAnual;
    return resultado; 

  }

  //Método para retirar Dinero 
  public double retirarDinero(double retirar){
    
    this.balance=balance-retirar;

    return balance;
  }

  //Métodod para depositar Dinero
  public double depositarDinero(double deposito){
    
    this.balance=balance+deposito;


    return balance;
  }



}

