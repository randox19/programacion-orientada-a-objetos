//Randall Zumbado Huertas
//2019082664

//Clase principal
class Main {

  //Método princiapl
  public static void main(String[] args) {
    
    //Objetos de prueba
    Cuenta c1 = new Cuenta(1122,500000);
    c1.settasaDeInteresAnual(4.5);
    Cuenta c2 = new Cuenta (2019,20000);
    System.out.println(" Cuenta: "
                        + c1.getid()
                        +" Balance: "
                        +c1.getbalance()
                        + " Interes Anual " 
                        + c1.gettasaDeInteresAnual()+"%"
                        +" Fecha de Creación: "
                        + c1.getfechaDeCreacion()
                        );

    c1.depositarDinero(150);

    System.out.println(" Cuenta: "
                        + c2.getid()
                        +" Balance: "
                        +c2.getbalance()
                        + " Interes Anual " 
                        + c2.gettasaDeInteresAnual()+"%"
                        +" Fecha de Creación: "
                        + c2.getfechaDeCreacion());
  

    //Crea un objeto de ATM para ejecutarlo
    ATM atm1= new ATM ();

    System.out.println(atm1);
  
  }
  
}  