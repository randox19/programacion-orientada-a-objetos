//Randall Zumbado Huertas
//2019082664


//Bibliotecas importadas
import java.util.Scanner;
import java.util.InputMismatchException;
public class ATM{  
      
    boolean salir = false; // Variable que controla que el id se repita
    int opcion; //Captura la opcion del usuario con Scanner de tipo entero
    double opcionDouble; //Captura la opcion del usuario pero de tipo double 
    boolean close = false;// Variable que controla que el menu se repita
  
  public ATM(){
    //Aca se define el arreglo de cuentas 10 en total.
      Cuenta[] arregloCuentas = {new Cuenta(0,100000),new Cuenta(1,100000), new Cuenta(2,100000),new Cuenta(3,100000),new Cuenta(4,100000),new Cuenta(5,100000),new Cuenta(6,100000),new Cuenta(7,100000),new Cuenta(8,100000),new Cuenta(9,100000)};   
    
    //Captura la opcion del usuario
    Scanner mi_opcion = new Scanner(System.in);
    
    //While que pide el ID despues lo compara con un switch y despues otro while adentro de ese para que se despliegue el menú el cual se creo a partir de otro switch.
    while (!salir){
        try{
          System.out.println("Ingrese su id: ");
          
          opcion=mi_opcion.nextInt();
          
          switch(opcion){
              case 0:
                System.out.println("El balance es de: "
                                  +arregloCuentas[0].getbalance());
                while(!close){
                  System.out.println("Menu Principal");
                  System.out.println("1. Ver balance actual");
                  System.out.println("2. Retirar Dinero");
                  System.out.println("3. Depositar Dinero");
                  System.out.println("4. Salir");
                  try{
                    System.out.println("Ingrese su selección: ");
                    opcion=mi_opcion.nextInt();
                    
                    switch(opcion){
                      case 1:
                        System.out.println("El balance es de: "
                                  +arregloCuentas[0].getbalance());
                        break;
                      
                      case 2:
                        System.out.println("Ingrese la cantidad para retirar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[0].retirarDinero(opcionDouble);
                        break;

                      case 3:
                        System.out.println("Ingrese la cantidad para depositar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[0].depositarDinero(opcionDouble);
                        
                        break;
                      
                      case 4:
                        
                        close= true;
                        break;

                      default:
                        System.out.println("Ingrese un número del 1 al 4");
                    
                    }
                  }catch(InputMismatchException e){
                    System.out.println("Debe ser un número");
                    mi_opcion.next();

                  }

                }
                close=false;
                break;
            
              case 1:
                System.out.println("El balance es de: "
                                  +arregloCuentas[1].getbalance());
                while(!close){
                  System.out.println("Menu Principal");
                  System.out.println("1. Ver balance actual");
                  System.out.println("2. Retirar Dinero");
                  System.out.println("3. Depositar Dinero");
                  System.out.println("4. Salir");
                  try{
                    System.out.println("Ingrese su selección: ");
                    opcion=mi_opcion.nextInt();
                    
                    switch(opcion){
                      case 1:
                        System.out.println("El balance es de: "
                                  +arregloCuentas[1].getbalance());
                        break;
                      
                      case 2:
                        System.out.println("Ingrese la cantidad para retirar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[1].retirarDinero(opcionDouble);
                        break;

                      case 3:
                        System.out.println("Ingrese la cantidad para depositar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[1].depositarDinero(opcionDouble);
                        
                        break;
                      
                      case 4:
                        
                        close= true;
                        break;

                      default:
                        System.out.println("Ingrese un número del 1 al 4");
                    
                    }
                  }catch(InputMismatchException e){
                    System.out.println("Debe ser un número");
                    mi_opcion.next();
                  }
                }
                close=false;
                break;
              
              case 2:
                System.out.println("El balance es de: "
                                  +arregloCuentas[2].getbalance());
                while(!close){
                  System.out.println("Menu Principal");
                  System.out.println("1. Ver balance actual");
                  System.out.println("2. Retirar Dinero");
                  System.out.println("3. Depositar Dinero");
                  System.out.println("4. Salir");
                  try{
                    System.out.println("Ingrese su selección: ");
                    opcion=mi_opcion.nextInt();
                    
                    switch(opcion){
                      case 1:
                        System.out.println("El balance es de: "
                                  +arregloCuentas[2].getbalance());
                        break;
                      
                      case 2:
                        System.out.println("Ingrese la cantidad para retirar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[2].retirarDinero(opcionDouble);
                        break;

                      case 3:
                        System.out.println("Ingrese la cantidad para depositar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[2].depositarDinero(opcionDouble);
                        
                        break;
                      
                      case 4:
                        
                        close= true;
                        break;

                      default:
                        System.out.println("Ingrese un número del 1 al 4");
                    
                    }
                  }catch(InputMismatchException e){
                    System.out.println("Debe ser un número");
                    mi_opcion.next();
                  }          
                }
                close=false;
                break;
              
              case 3:
                System.out.println("El balance es de: "
                                  +arregloCuentas[3].getbalance());
                while(!close){
                  System.out.println("Menu Principal");
                  System.out.println("1. Ver balance actual");
                  System.out.println("2. Retirar Dinero");
                  System.out.println("3. Depositar Dinero");
                  System.out.println("4. Salir");
                  try{
                    System.out.println("Ingrese su selección: ");
                    opcion=mi_opcion.nextInt();
                    
                    switch(opcion){
                      case 1:
                        System.out.println("El balance es de: "
                                  +arregloCuentas[3].getbalance());
                        break;
                      
                      case 2:
                        System.out.println("Ingrese la cantidad para retirar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[3].retirarDinero(opcionDouble);
                        break;

                      case 3:
                        System.out.println("Ingrese la cantidad para depositar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[3].depositarDinero(opcionDouble);
                        
                        break;
                      
                      case 4:
                        
                        close= true;
                        break;

                      default:
                        System.out.println("Ingrese un número del 1 al 4");
                    
                    }
                  }catch(InputMismatchException e){
                    System.out.println("Debe ser un número");
                    mi_opcion.next();
                  }          
                } 
                close=false;
                break;
              
              case 4:
                System.out.println("El balance es de: "
                                  +arregloCuentas[4].getbalance());
                while(!close){
                  System.out.println("Menu Principal");
                  System.out.println("1. Ver balance actual");
                  System.out.println("2. Retirar Dinero");
                  System.out.println("3. Depositar Dinero");
                  System.out.println("4. Salir");
                  try{
                    System.out.println("Ingrese su selección: ");
                    opcion=mi_opcion.nextInt();
                    
                    switch(opcion){
                      case 1:
                        System.out.println("El balance es de: "
                                  +arregloCuentas[4].getbalance());
                        break;
                      
                      case 2:
                        System.out.println("Ingrese la cantidad para retirar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[4].retirarDinero(opcionDouble);
                        break;

                      case 3:
                        System.out.println("Ingrese la cantidad para depositar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[4].depositarDinero(opcionDouble);
                        
                        break;
                      
                      case 4:
                        
                        close= true;
                        break;

                      default:
                        System.out.println("Ingrese un número del 1 al 4");
                    
                    }
                  }catch(InputMismatchException e){
                    System.out.println("Debe ser un número");
                    mi_opcion.next();
                  }
                }  
                close=false;
                break;
              
              case 5:
                System.out.println("El balance es de: "
                                  +arregloCuentas[5].getbalance());
                while(!close){
                  System.out.println("Menu Principal");
                  System.out.println("1. Ver balance actual");
                  System.out.println("2. Retirar Dinero");
                  System.out.println("3. Depositar Dinero");
                  System.out.println("4. Salir");
                  try{
                    System.out.println("Ingrese su selección: ");
                    opcion=mi_opcion.nextInt();
                    
                    switch(opcion){
                      case 1:
                        System.out.println("El balance es de: "
                                  +arregloCuentas[5].getbalance());
                        break;
                      
                      case 2:
                        System.out.println("Ingrese la cantidad para retirar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[5].retirarDinero(opcionDouble);
                        break;

                      case 3:
                        System.out.println("Ingrese la cantidad para depositar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[5].depositarDinero(opcionDouble);
                        
                        break;
                      
                      case 4:
                        
                        close= true;
                        break;

                      default:
                        System.out.println("Ingrese un número del 1 al 4");
                    
                    }
                  }catch(InputMismatchException e){
                    System.out.println("Debe ser un número");
                    mi_opcion.next();
                  }
                }  
                close=false;
                break;   
              
              case 6:
                System.out.println("El balance es de: "
                                  +arregloCuentas[6].getbalance());
                while(!close){
                  System.out.println("Menu Principal");
                  System.out.println("1. Ver balance actual");
                  System.out.println("2. Retirar Dinero");
                  System.out.println("3. Depositar Dinero");
                  System.out.println("4. Salir");
                  try{
                    System.out.println("Ingrese su selección: ");
                    opcion=mi_opcion.nextInt();
                    
                    switch(opcion){
                      case 1:
                        System.out.println("El balance es de: "
                                  +arregloCuentas[6].getbalance());
                        break;
                      
                      case 2:
                        System.out.println("Ingrese la cantidad para retirar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[6].retirarDinero(opcionDouble);
                        break;

                      case 3:
                        System.out.println("Ingrese la cantidad para depositar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[6].depositarDinero(opcionDouble);
                        
                        break;
                      
                      case 4:
                        
                        close= true;
                        break;

                      default:
                        System.out.println("Ingrese un número del 1 al 4");
                    
                    }
                  }catch(InputMismatchException e){
                    System.out.println("Debe ser un número");
                    mi_opcion.next();
                  }
                }
                close=false;
                break;
              
              case 7:
                System.out.println("El balance es de: "
                                  +arregloCuentas[7].getbalance());
                while(!close){
                  System.out.println("Menu Principal");
                  System.out.println("1. Ver balance actual");
                  System.out.println("2. Retirar Dinero");
                  System.out.println("3. Depositar Dinero");
                  System.out.println("4. Salir");
                  try{
                    System.out.println("Ingrese su selección: ");
                    opcion=mi_opcion.nextInt();
                    
                    switch(opcion){
                      case 1:
                        System.out.println("El balance es de: "
                                  +arregloCuentas[7].getbalance());
                        break;
                      
                      case 2:
                        System.out.println("Ingrese la cantidad para retirar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[7].retirarDinero(opcionDouble);
                        break;

                      case 3:
                        System.out.println("Ingrese la cantidad para depositar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[7].depositarDinero(opcionDouble);
                        
                        break;
                      
                      case 4:
                        
                        close= true;
                        break;

                      default:
                        System.out.println("Ingrese un número del 1 al 4");
                    
                    }
                  }catch(InputMismatchException e){
                    System.out.println("Debe ser un número");
                    mi_opcion.next();
                  }
                }
                close=false;
                break;    
              
              case 8:
                System.out.println("El balance es de: "
                                  +arregloCuentas[8].getbalance());
                while(!close){
                  System.out.println("Menu Principal");
                  System.out.println("1. Ver balance actual");
                  System.out.println("2. Retirar Dinero");
                  System.out.println("3. Depositar Dinero");
                  System.out.println("4. Salir");
                  try{
                    System.out.println("Ingrese su selección: ");
                    opcion=mi_opcion.nextInt();
                    
                    switch(opcion){
                      case 1:
                        System.out.println("El balance es de: "
                                  +arregloCuentas[8].getbalance());
                        break;
                      
                      case 2:
                        System.out.println("Ingrese la cantidad para retirar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[8].retirarDinero(opcionDouble);
                        break;

                      case 3:
                        System.out.println("Ingrese la cantidad para depositar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[8].depositarDinero(opcionDouble);
                        
                        break;
                      
                      case 4:
                        
                        close= true;
                        break;

                      default:
                        System.out.println("Ingrese un número del 1 al 4");
                    
                    }
                  }catch(InputMismatchException e){
                    System.out.println("Debe ser un número");
                    mi_opcion.next();
                  }
                }  
                close=false;
                break;    
            
              
              case 9:
                System.out.println("El balance es de: "
                                  +arregloCuentas[9].getbalance());
                while(!close){
                  System.out.println("Menu Principal");
                  System.out.println("1. Ver balance actual");
                  System.out.println("2. Retirar Dinero");
                  System.out.println("3. Depositar Dinero");
                  System.out.println("4. Salir");
                  try{
                    System.out.println("Ingrese su selección: ");
                    opcion=mi_opcion.nextInt();
                    
                    switch(opcion){
                      case 1:
                        System.out.println("El balance es de: "
                                  +arregloCuentas[9].getbalance());
                        break;
                      
                      case 2:
                        System.out.println("Ingrese la cantidad para retirar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[9].retirarDinero(opcionDouble);
                        break;

                      case 3:
                        System.out.println("Ingrese la cantidad para depositar: ");
                        opcionDouble=mi_opcion.nextDouble();
                        arregloCuentas[9].depositarDinero(opcionDouble);
                        
                        break;
                      
                      case 4:
                        
                        close= true;
                        break;

                      default:
                        System.out.println("Ingrese un número del 1 al 4");
                    
                    }
                  }catch(InputMismatchException e){
                    System.out.println("Debe ser un número");
                    mi_opcion.next();
                  }
                }  
                close=false;
                break;  
            
            default:
            System.out.println("Ingrese un número del 0 al 9");
          
          }
        }catch(InputMismatchException e){
          System.out.println("Debe ser un número");
          mi_opcion.next();
        }
        
    }
  }  


}



   