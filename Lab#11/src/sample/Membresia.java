package sample;
/************************
 Instituto Tecnológico de Costa Rica
 Programación Orientada a Objetos IC-2101
 II Semestre 2019
 Profesora: Samanta Ramijan Carmiol
 Estudiante: Randall Zumbado - 2019082664
 María José Barquero - 2019037947
 Te Chen - 2019033478
 ************************/
public class Membresia {
    private int id;
    private String nombre;
    private float porcentajeDescuento;
    private static int contador = 1;

    public Membresia(String nombre, float porcentajeDescuento) {
        this.nombre = nombre;
        this.porcentajeDescuento = porcentajeDescuento;
        this.id = contador++;
    }

    public String getNombre() {
        return nombre;
    }

    public float getPorcentajeDescuento() {
        return porcentajeDescuento;
    }

    @Override
    public String toString() {
        String info;
        info = String.format("\t%20s", this.nombre)
                + String.format("\t%10d%s", (int)(porcentajeDescuento*100),"% de descuento");
        return info;

    }
}