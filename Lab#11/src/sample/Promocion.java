package sample;
/************************
 Instituto Tecnológico de Costa Rica
 Programación Orientada a Objetos IC-2101
 II Semestre 2019
 Profesora: Samanta Ramijan Carmiol
 Estudiante: Randall Zumbado - 2019082664
 María José Barquero - 2019037947
 Te Chen - 2019033478
 ************************/
public class Promocion {
    private String nombre;
    private int cantidad;
    private float precio;
    private float descuento;

    private Producto producto;

    public Promocion(String nombre, int cantidad, float precio, Producto producto) {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.precio = precio;
        this.producto = producto;
        this.descuento = (producto.getPrecioConImpuesto()*cantidad) - precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public float getDescuento() {
        return descuento;
    }

    @Override
    public String toString() {
        return " ** " + " " + this.nombre;
    }
}