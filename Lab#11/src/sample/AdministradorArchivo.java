package sample;
/************************
 Instituto Tecnológico de Costa Rica
 Programación Orientada a Objetos IC-2101
 II Semestre 2019
 Profesora: Samanta Ramijan Carmiol
 Estudiante: Randall Zumbado - 2019082664
 María José Barquero - 2019037947
 Te Chen - 2019033478
 ************************/
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.util.ArrayList;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

public class AdministradorArchivo {


    private int ind=0;


    private ArrayList<String> productos = new ArrayList<String>();
    private ArrayList<Double> precio = new ArrayList<Double>();
    private ArrayList<Double> impuesto = new ArrayList<Double>();
    private ArrayList<String> tipoVenta = new ArrayList<String>();
    public AdministradorArchivo() {
        try {
            FileInputStream file = new FileInputStream("productos.xlsx");

            XSSFWorkbook libro = new XSSFWorkbook(file);

            XSSFSheet hoja = libro.getSheetAt(0);

            Iterator<Row> filas = hoja.iterator();
            Iterator<Cell> celdas;

            Row fila;
            Cell celda;
            while (filas.hasNext()) {
                while (ind < 3){
                    filas.next();
                    ind++;
                }


                fila = filas.next();
                celdas = fila.cellIterator();

                while (celdas.hasNext()) {
                    celda = celdas.next();

                    CellType type= celda.getCellType();

                        if( type==CellType.NUMERIC && celda.getColumnIndex()==1) {
                            double num = celda.getNumericCellValue();
                            precio.add(num);


                        }

                        else if (type==CellType.NUMERIC && celda.getColumnIndex()==2){
                            double num2 = celda.getNumericCellValue();
                            impuesto.add(num2);


                        }
                        else if (type == CellType.STRING && celda.getColumnIndex()==0){
                            String prod = celda.getStringCellValue();
                            productos.add(prod);

                        }
                        else if (type == CellType.STRING && celda.getColumnIndex()==3){
                            String tipo = celda.getStringCellValue();
                            tipoVenta.add(tipo);


                        }

                }

            }

            libro.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }


    }

    public ArrayList<Double> getImpuesto() {
        return impuesto;
    }

    public ArrayList<String> getProductos() {
        return productos;
    }

    public ArrayList<Double> getPrecio() {
        return precio;
    }

    public ArrayList<String> getTipoVenta() {
        return tipoVenta;
    }

    public void setImpuesto(double impues) {
        impuesto.add(impues);

    }

    public void setProductos(String product){
        productos.add(product);


    }

    public void setPrecio(double pre) {
        precio.add(pre);
    }

    public void setTipoVenta(String type) {
        tipoVenta.add(type);
    }
}