//Randall Zumbado Huertas
//2019082664

//Bibliotecas importadas.
import java.util.ArrayList;

//Clase PilaTEC.
public class PilaTEC extends ArrayList{

  //Constructor de PilaTEC.
  public PilaTEC(){
    super();
  }

  //Método cuando la pila está vacía.
  public boolean isEmpty(){
    return super.isEmpty();
  }

  // Método de optener el largo de la pila.
  public int getSize(){
    return super.size();
  }

  //Método de sacar el último elemento introducido de la pila
  //pero sin borrarlo de la pila.
  public Object peek(){
    return super.get(getSize()-1);
  }


  //Método de sacar el último elemento introducido de la pila
  //borrandolo de la pila.
  public Object pop(){
    Object o = super.get(getSize()-1); super.remove(getSize()-1);
    return o;
  }

  //Método para introducir un elemento en la pila.
  public void push(Object o){
    super.add(o);
  }

  //Sobreescritura del método toString.
  @Override
  public String toString(){
    return "pila: "+super.toString();

  }
}