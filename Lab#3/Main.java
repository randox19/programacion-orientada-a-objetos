//Randall Zumbado Huertas
//2019082664

//Importo bibliotecas.
import java.util.Scanner;

class Main {
  public static void main(String[] args) {
    
    private int contador=0; //Variable para salir del while.
    
    PilaTEC pila = new PilaTEC(); //Instancio un objeto tipo PilaTEC.

    Scanner dato = new Scanner(System.in); //Instancio el scaner para interactuar con el usuario.

    //Introduce los 5 númeors enteros
    while(contador != 5){
  
      System.out.println("Ingrese un número entero:");
  
      int entrada=dato.nextInt();
      
      contador=contador+1;

      pila.push(entrada);
    
    }

    //Saca los elementos de la pila.
    System.out.println("\nLos números ingresados son :\n");
    for(int i=0; i < 5; i++){
      System.out.print(pila.pop()+ " ");

    }
    
    
  
  }
}