/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Estudiantes: Randall Zumbado y Steven Alvarado.
    Profesora: Samanta Ramijan Carmiol
**********************************************************************/
import java.util.ArrayList;
public class Controlador implements OnOff {
    
    //Instancia un objeto Casa
    Casa casa=new Casa();
    
    //Variables a utilizar
    private int objeto;
    private String habitacion;

    //Constructor
    public Controlador(){
    }
    
    //Métodos abstractos del Interfaz
    @Override
    public String encendido(){

        return "Encendid@";

    }
    @Override
    public String apagado(){

        return "Apagad@";

    }

    //Agrega una habitación
    public void agregarHabitacion(String habitacion){
        casa.setCuarto(habitacion);;
    }
    // Obtiene todas las habitaciones registradas
    public ArrayList<String> getHabitaciones(){

        return casa.getLista();
    }

    //Obtiene una habitación en específico
    public String getHabitacion(int i){

        return casa.getCuarto(i);

    }

}
