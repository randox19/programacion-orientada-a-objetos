/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Estudiantes: Randall Zumbado y Steven Alvarado.
    Profesora: Samanta Ramijan Carmiol
**********************************************************************/
import java.util.ArrayList;



public class Casa{
    
    //Variables a utilizar
    private String cuarto;
    private int i;
    private ArrayList <String> list =new ArrayList <String> ();    
    
    //Constructor 
    public Casa(){


    }

    //Método para añadir un cuarto
    public void setCuarto(String cuarto){

        list.add(cuarto);

    }
    
    //Método para mostrar todos los cuartos
    public ArrayList <String> getLista(){

       return list;

    }

    //Método para obtener el nombre de un cuarto
    public String getCuarto(int i){


        return list.get(i);


    }
}
