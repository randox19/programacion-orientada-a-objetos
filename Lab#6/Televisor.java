/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Estudiantes: Randall Zumbado y Steven Alvarado.
    Profesora: Samanta Ramijan Carmiol
**********************************************************************/
public class Televisor{
    
    //Variables a utilizar
    private String bajarCanal;
    private String subirCanal;
    private String bajarVolumen;
    private String subirVolumen;
    private String Name="Televisor";

    //Constructo
    public Televisor(){

    }

    //Método para obtener el nombre de la clase
    public String getNombre(){

        return Name;


    }

    //Métodos para obtener diferentes acciones de la clase
    public String getSubirCanal() {
        return subirCanal;
    }

    public String getBajarCanal() {
        return bajarCanal;
    }

    public String getBajarVolumen() {
        return bajarVolumen;
    }

    public String getSubirVolumen() {
        return subirVolumen;
    }

    //Métodos para asignar diferentes acciones a la clase
    public void setSubirCanal(String subirCanal) {
        this.subirCanal = subirCanal;
    }

    public void setBajarCanal(String bajarCanal) {
        this.bajarCanal = bajarCanal;
    }

    public void setSubirVolumen(String subirVolumen) {
        this.subirVolumen = subirVolumen;
    }

    public void setBajarVolumen(String bajarVolumen) {
        this.bajarVolumen = bajarVolumen;
    }
}