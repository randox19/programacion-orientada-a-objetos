/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Estudiantes: Randall Zumbado y Steven Alvarado.
    Profesora: Samanta Ramijan Carmiol
**********************************************************************/
public class Computadora{
    
    //Variables a utilizar
    private String suspender;
    private String reiniciar;
    private String bloquear;
    private String bajarVolumen;
    private String subirVolumen;
    private String bajarBrillo;
    private String subirBrillo;
    private String Name="Computadora";

    //Constructor
    public Computadora(){

    }
    
    //Método para obtener el nombre de la clase
    public String getNombre(){

        return Name;


    }
    
    //Métodos para obtener diferentes acciones de la Computadora
    public String getSuspender() {
        return suspender;
    }

    public String getReiniciar() {
        return reiniciar;
    }

    public String getBloquear() {
        return bloquear;
    }

    public String getSubirVolumen() {
        return subirVolumen;
    }

    public String getBajarVolumen() {
        return bajarVolumen;
    }

    public String getSubirBrillo() {
        return subirBrillo;
    }

    public String getBajarBrillo() {
        return bajarBrillo;
    }

    //Métodos para asignar diferentes acciones a la Computadora
    public void setSuspender(String suspender) {
        this.suspender = suspender;
    }

    public void setReiniciar(String reiniciar) {
        this.reiniciar = reiniciar;
    }

    public void setBloquear(String bloquear) {
        this.bloquear = bloquear;
    }

    public void setSubirVolumen(String subirVolumen) {
        this.subirVolumen = subirVolumen;
    }

    public void  setBajarVolumen(String bajarVolumen) {
        this.bajarVolumen = bajarVolumen;
    }
    public void  setSubirBrillo(String subirBrillo) {
        this.subirBrillo = subirBrillo;
    }

    public void  setBajarBrillo(String bajarBrillo) {
        this.bajarBrillo = bajarBrillo;
    }
}