/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Estudiantes: Randall Zumbado y Steven Alvarado.
    Profesora: Samanta Ramijan Carmiol
**********************************************************************/
public class Ducha{
    //Variables a Utilizar
    private String frio;
    private String tibio;
    private String caliente;
    private String Name="Ducha";

    //Constructor
    public Ducha (){

    }
    //Método para obtener el nombre de la clase
    public String getNombre(){

        return Name;


    }
    
    //Métodos para obtener diferentes acciones de la clase
    public String getFrio() {
        return frio;
    }

    public String getTibio() {
        return tibio;
    }

    public String getCaliente() {
        return caliente;
    }

    //Métodos para asignar diferentes acciones de la clase
    public void setFrio(String frio) {
        this.frio = frio;
    }

    public void setTibio(String tibio) {
        this.tibio = tibio;
    }

    public void setCaliente(String caliente) {
        this.caliente = caliente;
    }
}

