/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Estudiantes: Randall Zumbado y Steven Alvarado.
    Profesora: Samanta Ramijan Carmiol
**********************************************************************/
public class DVD{
    
    //Variables a utilizar
    private String abrirBandeja;
    private String cerrarBandeja;
    private String adelantarVideo;
    private String atrasarVideo;
    private String siguiente;
    private String anterior;
    private String bajarVolumen;
    private String subirVolumen;
    private String Name="DVD";

    //Constructor
    public DVD(){

    }

    //Método para obtener el nombre de la clase
    public String getNombre(){

        return Name;


    }

    //Métodos para obtener diferentes acciones de la clase
    public String getAbrirBandeja() {
        return abrirBandeja;
    }

    public String getCerrarBandeja() {
        return cerrarBandeja;
    }

    public String getAdelantarVideo() {
        return adelantarVideo;
    }

    public String getAtrasarVideo() {
        return atrasarVideo;
    }

    public String getSiguiente() {
        return siguiente;
    }

    public String getAnterior() {
        return anterior;
    }

    public String getBajarVolumen() {
        return bajarVolumen;
    }

    public String getSubirVolumen() {
        return subirVolumen;
    }

    //Métodos para asignar diferentes acciones a la clase
    public void setAbrirBandeja(String abrirBandeja) {
        this.abrirBandeja = abrirBandeja;
    }

    public void setCerrarBandeja(String cerrarBandeja) {
        this.cerrarBandeja = cerrarBandeja;
    }

    public void  setAdelantarVideo(String adelantarVideo) {
        this.adelantarVideo = adelantarVideo;
    }

    public void  setAtrasarVideo(String atrasarVideo) {
        this.atrasarVideo = atrasarVideo;
    }

    public void  setSiguiente(String siguiente) {
        this.siguiente = siguiente;
    }

    public void  setAnterrior(String anterior) {
        this.anterior = anterior;
    }

    public void  setSubirVolumen(String subirVolumen) {
        this.subirVolumen = subirVolumen;
    }

    public void  setBajarVolumen(String bajarVolumen) {
        this.bajarVolumen = bajarVolumen;
    }
}