/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Estudiantes: Randall Zumbado y Steven Alvarado.
    Profesora: Samanta Ramijan Carmiol
**********************************************************************/
public class Secadora{

    //Variables a utilizar
    private String suspender;
    private String secadoNormal;
    private String secadoCaliente;
    private String Name="Secadora";

    //Constructor
    public Secadora(){
    

    }

    //Método para obtener el nombre de la clase
    public String getNombre(){

        return Name;


    }

    //Métodos para obtener diferentes acciones de la clase
    public String getSuspender() {
        return suspender;
    }

    public String getSecadoNormal() {
        return secadoNormal;
    }

    public String getSecadoCaliente() {
        return secadoCaliente;
    }

    //Métodos para asignar diferentes acciones a la clase
    public void setSecadoNormal(String secadoNormal) {
        this.secadoNormal = secadoNormal;
    }

    public void setSecadoCaliente(String secadoCaliente) {
        this.secadoCaliente = secadoCaliente;
    }
}   