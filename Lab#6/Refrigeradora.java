/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Estudiantes: Randall Zumbado y Steven Alvarado.
    Profesora: Samanta Ramijan Carmiol
**********************************************************************/
public class Refrigeradora{
    
    //Variables a utilizar
    private String bajarTemperatura;
    private String subirTemperatura;
    private String Name="Refrigeradora";
    
    //Contructor 
    public Refrigeradora(){
    }
    
    //Método para obtener el nombre de la clase
    public String getNombre(){

        return Name;


    }

    ////Métodos para asignar diferentes acciones a la clase
    public void setSubirTemperatura(String subirTemperatura) {
        this.subirTemperatura = subirTemperatura;
    }

    public void setBajarTemperatura(String bajarTemperatura) {
        this.bajarTemperatura = bajarTemperatura;
    }

    ////Métodos para obtener diferentes acciones de la clase
    public String getSubirTemperatura(){

        return subirTemperatura;
    }
    public String getBajarTemperatura(){

        return bajarTemperatura;
    }
}